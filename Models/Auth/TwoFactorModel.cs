namespace Sts.Models.Auth
{
    public class TwoFactorModel
    {
        public string ReturnUrl { get; set; }

        public string Code { get; set; }

        public string Provider { get; set; }

        public string ProviderTitle { get; set; }

        public string ProviderInstructions { get; set; }
    }
}
