namespace Sts.Models.Account
{
    public class ChangePasswordModel
    {
        public string CurrentPassword { get; set; }
        
        public string NewPassword1 { get; set; }

        public string NewPassword2 { get; set; }
    }
}