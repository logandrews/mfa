using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sts.IdentityCore.Models;
using Sts.IdentityCore.Stores;
using Sts.IdentityCore.TokenProvider;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authorization;
using YubicoClient;

namespace Sts
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ConstantTwoFactorTokenProviderOptions>(Configuration.GetSection("ConstantTwoFactorAuthentication"));

            services.Configure<YubicoOptions>(options => {
                options.ClientId = Configuration.GetValue<string>("yubicloud:ClientId");
                options.SecretKey = Convert.FromBase64String(Configuration.GetValue<string>("yubicloud:SecretKey"));
            });

            services.AddHttpClient<YubicoService>();

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddUserStore<ApplicationUserStore>()
                .AddRoleStore<ApplicationRoleStore>()
                .AddTokenProvider<ConstantTwoFactorTokenProvider>(ConstantTwoFactorTokenProvider.ProviderName)
                .AddTokenProvider<YubikeyTwoFactorTokenProvider>(YubikeyTwoFactorTokenProvider.ProviderName);

            services.Configure<IdentityOptions>(options => {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 0;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;

                options.ClaimsIdentity.UserIdClaimType = "sub";
                options.ClaimsIdentity.UserNameClaimType = "name";
                options.ClaimsIdentity.SecurityStampClaimType = "sstamp";
                options.ClaimsIdentity.RoleClaimType = "role";
            });

            services.ConfigureApplicationCookie(options => {
                options.LoginPath = "/login";

                options.Cookie.HttpOnly = true;
                options.Cookie.Name = "auth";
            });

            services.Configure<AntiforgeryOptions>(options => {
                options.Cookie.Name = "xsrf";
                options.Cookie.HttpOnly = true;
            });

            // services.AddAuthentication()
            //     .AddIdentityCookies(options => {
            //         options.TwoFactorUserIdCookie.Configure(options => {
            //             options.Cookie.Name = "mfa-id";
            //         });

            //         options.TwoFactorRememberMeCookie.Configure(options => {
            //             options.Cookie.Name = "mfa-rm";
            //         });
            //     });
            
            // services.AddHttpContextAccessor();

            // services
            //     .AddIdentityCore<User>()
            //     .AddUserStore<UserStore>();
            
            // services.AddScoped<SignInManager<User>>();

            // services
            //     .AddAuthentication(options => {
            //         options.DefaultScheme = IdentityConstants.ApplicationScheme;
            //     })
            //     .AddCookie(IdentityConstants.ApplicationScheme, options => {
            //         options.LoginPath = "/login";
            //         options.ReturnUrlParameter = "";
            //     })
            //     .AddCookie(IdentityConstants.ExternalScheme)
            //     .AddCookie(IdentityConstants.TwoFactorUserIdScheme)
            //     .AddCookie(IdentityConstants.TwoFactorRememberMeScheme);

            services.AddControllersWithViews(options => {

                var defaultPolicy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();

                options.Filters.Add(new AuthorizeFilter(defaultPolicy));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
