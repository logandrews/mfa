using System;
using System.Security.Cryptography;
using System.Text;

namespace YubicoClient
{
    public static class StringExtensions
    {

        public static string HmacSha1(this string s, byte[] secret)
        {
            return s.HmacSha1(secret, Encoding.ASCII);
        }

        public static string HmacSha1(this string s, byte[] secret, Encoding encoding)
        {
            var bytes = encoding.GetBytes(s);

            byte[] hmac;
            using (var hmacer = new HMACSHA1(secret))
            {
                hmac = hmacer.ComputeHash(bytes);
            }

            return Convert.ToBase64String(hmac);
        }
    }
}