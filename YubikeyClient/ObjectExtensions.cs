using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace YubicoClient
{
    public static class ObjectExtensions
    {
        public static string UrlEncode<T>(this T o)
        {
            return WebUtility.UrlEncode(o.ToString());
        }
    }
}