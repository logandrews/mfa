using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace YubicoClient
{
    [Flags]
    public enum QueryStringOptions
    {
        EscapeNone = 0,
        EscapeKey = 1,
        EscapeValue = 2,
        EscapeBoth = EscapeKey | EscapeValue,
    }
    
    public static class DictionaryExtensions
    {
        public static string ToQueryString<TKey, TValue>(this IDictionary<TKey, TValue> d)
        {
            return d.ToQueryString(QueryStringOptions.EscapeBoth);
        }

        public static string ToQueryString<TKey, TValue>(this IDictionary<TKey, TValue> d, QueryStringOptions options)
        {
            var escapeKey = (options & QueryStringOptions.EscapeKey) != 0;
            var escapeValue = (options & QueryStringOptions.EscapeValue) != 0;

            var queryParams = d.Select(kvp => {
                var key = escapeKey ? kvp.Key.UrlEncode() : kvp.Key.ToString();
                var value = escapeValue ? kvp.Value.UrlEncode() : kvp.Value.ToString();

                return $"{key}={value}";
            });

            return string.Join("&", queryParams);
        }
    }
}