namespace YubicoClient
{
    public class YubicoOptions
    {
        public string ClientId { get; set; }

        public byte[] SecretKey { get; set; }
    }
}