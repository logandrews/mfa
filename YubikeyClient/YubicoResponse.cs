namespace YubicoClient
{
    public class YubicoResponse
    {
        public string Otp { get; set; }

        public string Nonce { get; set; }

        public string Hmac { get; set; }

        public string Timestamp { get; set; }

        public string Status { get; set; }

        public string YubikeyTimestamp { get; set; }

        public string YubikeyCounter { get; set; }

        public string YubikeyUsage { get; set; }

        public int SyncLevel { get; set; }
    }
}