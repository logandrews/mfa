using System.Net.Http;
using System.Collections.Generic;
using System.Security.Cryptography;
using System;
using System.Threading.Tasks;
using System.Text;
using System.IO;
using Microsoft.Extensions.Options;

namespace YubicoClient
{
    public class YubicoService
    {
        private readonly YubicoOptions _options;
        
        private readonly HttpClient _client;

        public YubicoService(HttpClient client, IOptions<YubicoOptions> options)
        {
            _options = options.Value;
            _client = client;
        }

        public async Task<YubicoResponse> ValidateOtpAsync(string otp, bool includeTimestamp)
        {
            var nonce = CreateNonce();
            var parameters = new SortedDictionary<string, string> {
                { "otp", otp },
                { "nonce", nonce },
                { "id", _options.ClientId },
            };

            if (includeTimestamp) {
                parameters["timestamp"] = "1";
            }

            if (_options.SecretKey != null)
            {
                parameters["h"] = parameters.ToQueryString().HmacSha1(_options.SecretKey);
            }

            var url = "https://api.yubico.com/wsapi/2.0/verify?" + parameters.ToQueryString();

            var response = await _client.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var responseParameters = await ParseResponse(await response.Content.ReadAsStreamAsync());

            if (responseParameters["otp"] != otp)
            {
                throw new Exception("OTP between request and response does not match.");
            }

            if (responseParameters["nonce"] != nonce)
            {
                throw new Exception("Nonce between request and response does not match.");
            }

            string responseHmac = null;
            if (responseParameters.ContainsKey("h"))
            {
                responseHmac = responseParameters["h"];
                responseParameters.Remove("h");
            }

            var calcedHmac = responseParameters.ToQueryString(QueryStringOptions.EscapeNone).HmacSha1(_options.SecretKey);

            if (calcedHmac != responseHmac)
            {
                throw new Exception("HMAC in response is not valid.");
            }

            var yubicoResponse = new YubicoResponse {
                Otp = responseParameters["otp"],
                Nonce = responseParameters["nonce"],
                Timestamp = responseParameters["t"],
                Status = responseParameters["status"],
                SyncLevel = int.Parse(responseParameters.GetValueOrDefault("sl") ?? "-1"),

                Hmac = responseParameters.GetValueOrDefault("h"),

                YubikeyTimestamp = responseParameters.GetValueOrDefault("timestamp"),
                YubikeyCounter = responseParameters.GetValueOrDefault("sessioncounter"),
                YubikeyUsage = responseParameters.GetValueOrDefault("sessionuse"),
            };

            return yubicoResponse;
        }

        private static async Task<SortedDictionary<string, string>> ParseResponse(Stream stream)
        {
            var dict = new SortedDictionary<string, string>();

            using (var sr = new StreamReader(stream))
            {
                while (sr.Peek() > 0)
                {
                    var line = await sr.ReadLineAsync();

                    if (string.IsNullOrEmpty(line)) { continue; }
                    
                    var split = line.Split("=", 2);
                    dict.Add(split[0], split[1]);
                }
            }

            return dict;
        }

        private static string CreateNonce()
        {
            var bytes = new byte[16];
            RandomNumberGenerator.Fill(bytes);
            return BitConverter.ToString(bytes).Replace("-", "");
        }
    }
}