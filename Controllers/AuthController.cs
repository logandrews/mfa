using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;
using Sts.IdentityCore.Models;
using Sts.Models.Auth;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;

namespace Sts.Controllers
{
    public class AuthController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly UserManager<ApplicationUser> _userManager;

        public AuthController(ILogger<HomeController> logger, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager, IOptions<YubicoClient.YubicoOptions> yOptions)
        {
            _logger = logger;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("login")]
        public async Task<IActionResult> Login(string returnUrl)
        {
            var model = new LoginModel {
                ReturnUrl = returnUrl,
            };

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginPost(LoginModel model)
        {
            var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, true, false);
            
            if (result.Succeeded)
            {
                if (!string.IsNullOrEmpty(model.ReturnUrl))
                {
                    return LocalRedirectPreserveMethod(model.ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            if (result.RequiresTwoFactor)
            {
                return RedirectToAction(nameof(TwoFactor), new { ReturnUrl = model.ReturnUrl });
            }

            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("mfa")]
        public async Task<IActionResult> TwoFactor(string returnUrl)
        {
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

            if (user == null)
            {
                return RedirectToAction("Login");
            }

            var model = new TwoFactorModel
            {
                ReturnUrl = returnUrl,
                Provider = user.TwoFactorProvider,
                ProviderTitle = "Two Factor",
                ProviderInstructions = "Please provide your second factor code.",
            };

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("mfa")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> TwoFactorPost(TwoFactorModel model)
        {
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

            var result = await _signInManager.TwoFactorSignInAsync(user.TwoFactorProvider, model.Code, false, false);

            if (result.Succeeded)
            {
                if (string.IsNullOrEmpty(model.ReturnUrl))
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return LocalRedirect(model.ReturnUrl);
                }
            }

            model.Code = null;

            return View("TwoFactor", model);
        }

        [HttpGet]
        [Route("logout")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return RedirectToAction("Login");
        }
    }
}
