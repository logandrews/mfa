using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;
using Sts.IdentityCore.Models;
using Sts.Models.Auth;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authorization;
using Sts.Models.Account;
using Microsoft.Extensions.Options;
using Sts.IdentityCore.TokenProvider;
using YubicoClient;

namespace Sts.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;

        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly YubicoService _yubicoService;

        public AccountController(ILogger<AccountController> logger, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager, YubicoService yubicoService)
        {
            _logger = logger;
            _signInManager = signInManager;
            _userManager = userManager;
            _yubicoService = yubicoService;
        }
        
        [HttpGet]
        [Route("account")]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);

            var model = new IndexModel
            {
                TwoFactorProvider = user.TwoFactorProvider,
            };

            return View(model);
        }

        [HttpGet]
        [Route("account/password")]
        public async Task<IActionResult> ChangePassword()
        {
            var model = new ChangePasswordModel();

            return View(model);
        }

        [HttpPost]
        [Route("account/password")]
        public async Task<IActionResult> ChangePasswordPost(ChangePasswordModel model)
        {
            var user = await _userManager.GetUserAsync(User);

            if (model.NewPassword1 != model.NewPassword2)
            {
                return View("ChangePassword", new ChangePasswordModel());
            }

            var identityResult = await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword1);

            if (identityResult == IdentityResult.Success)
            {
                return RedirectToAction("Index", "Account");
            }

            return View("ChangePassword", new ChangePasswordModel());
        }

        [HttpGet]
        [Route("account/enable2fa")]
        public async Task<IActionResult> EnableTwoFactor()
        {
            var user = await _userManager.GetUserAsync(User);

            if (user.HasTwoFactorEnabled)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpGet]
        [Route("account/disable2fa")]
        public async Task<IActionResult> DisableTwoFactor()
        {
            var user = await _userManager.GetUserAsync(User);

            if (!user.HasTwoFactorEnabled)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpPost]
        [Route("account/disable2fa")]
        public async Task<IActionResult> DisableTwoFactorPost(DisableTwoFactorModel model)
        {
            var user = await _userManager.GetUserAsync(User);

            if (!user.HasTwoFactorEnabled)
            {
                return RedirectToAction("Index");
            }

            var validCode = await _userManager.VerifyTwoFactorTokenAsync(user, user.TwoFactorProvider, model.Code);

            if (validCode)
            {
                user.HasTwoFactorEnabled = false;
                user.TwoFactorProvider = null;

                await _userManager.UpdateAsync(user);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Route("account/enableconstant")]
        public async Task<IActionResult> EnableConstant()
        {
            var user = await _userManager.GetUserAsync(User);

            if (user.HasTwoFactorEnabled)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpPost]
        [Route("account/enableconstant")]
        public async Task<IActionResult> EnableConstantPost(EnableConstantModel model)
        {
            var user = await _userManager.GetUserAsync(User);

            if (user.HasTwoFactorEnabled)
            {
                return RedirectToAction("Index");
            }

            var validCode = await _userManager.VerifyTwoFactorTokenAsync(user, "constant", model.Code);

            if (validCode)
            {
                user.HasTwoFactorEnabled = true;
                user.TwoFactorProvider = ConstantTwoFactorTokenProvider.ProviderName;

                await _userManager.UpdateAsync(user);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Route("account/enableyubikey")]
        public async Task<IActionResult> EnableYubikey()
        {
            var user = await _userManager.GetUserAsync(User);

            if (user.HasTwoFactorEnabled)
            {
                return RedirectToAction("Index");
            }

            return View();;
        }

        [HttpPost]
        [Route("account/enableyubikey")]
        public async Task<IActionResult> EnableYubikeyPost(EnableYubikeyModel model)
        {
            var user = await _userManager.GetUserAsync(User);

            if (user.HasTwoFactorEnabled)
            {
                return RedirectToAction("Index");
            }

            var response = await _yubicoService.ValidateOtpAsync(model.Code, false);

            if (response.Status != "OK")
            {
                return RedirectToAction("EnableTwoFactor");
            }

            var yubikeyId = model.Code.Substring(0, 12);

            user.HasTwoFactorEnabled = true;
            user.TwoFactorProvider = "yubikey";
            user.YubikeyId = yubikeyId;

            await _userManager.UpdateAsync(user);
            
            return RedirectToAction("Index");
        }
    }
}
