
using System.Threading;
using System.Threading.Tasks;
using Sts.IdentityCore.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Sts.IdentityCore.TokenProvider
{
    public class ConstantTwoFactorTokenProviderOptions
    {
        public string Constant { get; set; }
    }
}
