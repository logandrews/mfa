
using System.Threading;
using System.Threading.Tasks;
using Sts.IdentityCore.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.Extensions.Options;
using YubicoClient;

namespace Sts.IdentityCore.TokenProvider
{
    public class YubikeyTwoFactorTokenProvider : IUserTwoFactorTokenProvider<ApplicationUser>
    {
        public static readonly string ProviderName = "yubikey";

        private readonly YubicoService _yubicoService;

        public YubikeyTwoFactorTokenProvider(YubicoService yubicoService)
        {
            _yubicoService = yubicoService;
        }

        public Task<bool> CanGenerateTwoFactorTokenAsync(UserManager<ApplicationUser> manager, ApplicationUser user)
        {
            return Task.FromResult(user.TwoFactorProvider == ProviderName);
        }

        public Task<string> GenerateAsync(string purpose, UserManager<ApplicationUser> manager, ApplicationUser user)
        {
            return Task.FromResult(string.Empty);
        }

        public async Task<bool> ValidateAsync(string purpose, string token, UserManager<ApplicationUser> manager, ApplicationUser user)
        {
            if (!token.StartsWith(user.YubikeyId))
            {
                return false;
            }
            
            var response = await _yubicoService.ValidateOtpAsync(token, false);

            return response.Status == "OK";
        }
    }
}
