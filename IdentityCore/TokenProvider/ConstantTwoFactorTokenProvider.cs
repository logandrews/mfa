
using System.Threading;
using System.Threading.Tasks;
using Sts.IdentityCore.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.Extensions.Options;

namespace Sts.IdentityCore.TokenProvider
{
    public class ConstantTwoFactorTokenProvider : IUserTwoFactorTokenProvider<ApplicationUser>
    {
        public static readonly string ProviderName = "constant";

        private readonly string _constant;

        public ConstantTwoFactorTokenProvider(IOptions<ConstantTwoFactorTokenProviderOptions> options)
        {
            _constant = options.Value.Constant;
        }

        public Task<bool> CanGenerateTwoFactorTokenAsync(UserManager<ApplicationUser> manager, ApplicationUser user)
        {
            return Task.FromResult(true);
        }

        public Task<string> GenerateAsync(string purpose, UserManager<ApplicationUser> manager, ApplicationUser user)
        {
            return Task.FromResult(string.Empty);
        }

        public Task<bool> ValidateAsync(string purpose, string token, UserManager<ApplicationUser> manager, ApplicationUser user)
        {
            return Task.FromResult(token.Equals(_constant, StringComparison.InvariantCulture));
        }
    }
}
