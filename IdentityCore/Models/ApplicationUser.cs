using System;

namespace Sts.IdentityCore.Models
{
    public class ApplicationUser
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string NormalizedUserName { get; set; }

        public string Password { get; set; }

        public bool HasTwoFactorEnabled { get; set; }
        
        public string TwoFactorProvider { get; set; }

        public bool LockoutEnabled { get; set; }

        public int FailedLoginCount { get; set; }

        public DateTimeOffset? LockoutEndDate { get; set; }

        public string YubikeyId { get; set; }
    }
}