namespace Sts.IdentityCore.Models
{
    public class ApplicationRole
    {
        public int Id { get; set; }

        public string RoleName { get; set; }

        public string NormalizedRoleName { get; set; }
    }
}